<?php

// Lista de tipos de arquivos permitidos
  $tiposPermitidos= array('application/msword', 'application/pdf', 'image/jpeg'); //apenas doc e pdf, 'image/jpeg', 'image/pjpeg', 'image/png');

  $name = $_POST['nome'];
   
  // O nome original do arquivo no computador do usuario
  $arqName = $_FILES['arquivo']['name'];
  
  // O tipo mime do arquivo. Um exemplo pode ser "image/gif"
  $arqType = $_FILES['arquivo']['type'];
 
 // O tamanho, em bytes, do arquivo
  $arqSize = $_FILES['arquivo']['size'];
 
 // O nome temporario do arquivo, como foi guardado no servidor
  $arqTemp = $_FILES['arquivo']['tmp_name'];
 
 // O codigo de erro associado a este upload de arquivo
  $arqError = $_FILES['arquivo']['error'];
  
  if ($arqError == 0) {
    
	   // Verifica o tipo de arquivo enviado
       if (array_search($arqType, $tiposPermitidos) === false) {
         echo '<script>alert("Tipo de arquivo invalido")</script>';
	     echo '<script>window.location = "./z3.php";</script>';
        
			// Não houveram erros, move o arquivo
		} else {
				$pasta = './000/';
	  
				// Pega a extensão do arquivo enviado
				$extensao = strtolower(end(explode('.', $arqName)));
      
				// Define o novo nome do arquivo usando um UNIX TIMESTAMP
				//$nome = time() . '.' . $extensao;
                $nome = time().'_'.date('Y'._.'m'._.'d') . '.' . $extensao;
				$upload = move_uploaded_file($arqTemp, $pasta . $nome);
				
				echo '<script>alert("ARQUIVO ENVIADO")</script>';
				echo '<script>window.location = "./z3.php";</script>';
			}
	
  } else {
	//  echo "erro ao enviar arquivo";
	echo '<script>alert("erro ao enviar arquivo")</script>';
	echo '<script>window.location = "./z3.php";</script>';
	}
	
	
	// adicionar popup para enviar arquivos 
	//header("Location: z3.php");
	
  ?>
  
  